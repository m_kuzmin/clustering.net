﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Clustering
{
	public class PointCloud<T> : List<T> where T : Point
    {
    	private readonly int dimension;
    	private Point centroid;
    	int k = 0;
    	
        #region Properties

        /// <summary>
        /// Центр облака
        /// </summary>
        public Point Centroid 
        {
        	get 
        	{
        		if (centroid == null) 
        		{
        			UpdateCentroid();
        		}
        		return centroid;
        	} 
        }
        public int Dimension { get { return dimension; } }

        #endregion

        #region Constructors

        public PointCloud(int dimension)
            : base()
        {
        	this.dimension = dimension;
            //this.centroid = new Point(dimension);
        }

        #endregion

        #region Methods
        
        public void Save(string filename)
        {
        	using( StreamWriter sw = new StreamWriter(filename) )
        	{
        		foreach (var e in this) 
        		{
        			foreach (var i in e.Coordinates) 
        			{
        				sw.Write(i);
        				sw.Write(' ');
        			}
        			sw.WriteLine();
        		}
        	}
        }

        public new void Add(T p)
        {
        	if (Dimension != p.Dimension)
        		throw new IndexOutOfRangeException("Размерность точки должна совпадать с размерностью облака");
        	base.Add(p);
            //UpdateCentroid();
            centroid = null;
        }        
        
        public new void AddRange(IEnumerable<T> collection)
        {
        	base.AddRange(collection);
        	//UpdateCentroid();
        	centroid = null;
        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            //UpdateCentroid();
            centroid = null;
		}

        public new void Remove(T p)
        {
            base.Remove(p);
            //UpdateCentroid();
            centroid = null;
        }

        public T GetPointNearestToCentroid()
        {
            double minimumDistance = 0.0;
            int nearestPointIndex = -1;

            foreach (var p in this)
            {
                double distance = p.DistanceTo(Centroid);

                if (this.IndexOf(p) == 0)
                {
                    minimumDistance = distance;
                    nearestPointIndex = this.IndexOf(p);
                }
                else
                {
                    if (minimumDistance > distance)
                    {
                        minimumDistance = distance;
                        nearestPointIndex = this.IndexOf(p);
                    }
                }
            }

            return (this[nearestPointIndex]);
        }       
        
        public List<PointCloud<T>> KMeansClustering( int clusterCount )
        {
            //divide points into equal clusters
            List<PointCloud<T>> allClusters = new List<PointCloud<T>>();
            List<List<T>> allGroups = this.SplitList(clusterCount);
            foreach (var group in allGroups)
            {
                PointCloud<T> cluster = new PointCloud<T>(this.Dimension);
                cluster.AddRange(group);
                allClusters.Add(cluster);
            }

            //start k-means clustering
            int movements = 1;
            while (movements > 0)
            {
                movements = 0;

                foreach (var cluster in allClusters) //for all clusters
                {
                    for (int pointIndex = 0; pointIndex < cluster.Count; pointIndex++) //for all points in each cluster
                    {
                        var point = cluster[pointIndex];

                        int nearestCluster = FindNearestCluster(allClusters, point);
                        if (nearestCluster != allClusters.IndexOf(cluster)) //if point has moved
                        {
                            if (cluster.Count > 1) //cluster shall have minimum one point
                            {
                                cluster.Remove(point);
                                allClusters[nearestCluster].Add(point);
                                movements++;
                            }
                        }
                    }
                }
            }

            return (allClusters);
        }

        #endregion

        #region Internal-Methods

        public void UpdateCentroid()
        {
        	this.centroid = new Point(dimension);
        	foreach (var p in this)
        	{
        		for (int i = 0; i < Dimension; i++)
        		{
        			centroid.Coordinates[i] += p.Coordinates[i];
        		}
        	}
        	
        	for (int i = 0; i < Dimension; i++)
        	{
        		centroid.Coordinates[i] /= Count;
        	}
        }
        
        private static int FindNearestCluster(List<PointCloud<T>> allClusters, T point)
        {
            double minimumDistance = 0;
            int nearestClusterIndex = -1;

            for (int k = 0; k < allClusters.Count; k++) //find nearest cluster
            {
                double distance = point.DistanceTo(allClusters[k].Centroid);
                if (k == 0)
                {
                    minimumDistance = distance;
                    nearestClusterIndex = 0;
                }
                else if (minimumDistance > distance)
                {
                    minimumDistance = distance;
                    nearestClusterIndex = k;
                }
            }

            return (nearestClusterIndex);
        }

        #endregion
    }
}
