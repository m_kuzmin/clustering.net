﻿using System;
using System.Text;

namespace Clustering
{
	public class Point
    {		
		public double[] Coordinates { get; protected set; }

		public int Dimension
		{
			get
			{
				if ( Coordinates != null)
					return Coordinates.GetLength(0);
				return 0;
			}
		}
		
        public Point(int dimension)
        {
        	Coordinates = new double[dimension];
        }

        /// <param name="coordinates">сохраняется ссылка</param>
        public Point(double[] coordinates)
        {
            Coordinates = coordinates;
        }

        public bool IsBetween(Point p1, Point p2)
        {
        //	try
        	{
        		for (int i = 0; i < Dimension; i++)
        		{
        			if ( !(this.Coordinates[i] >= p1.Coordinates[i] && this.Coordinates[i] <= p2.Coordinates[i]) )
        				return false;
        		}
        		return true;
        	}
        }

       /*public override bool Equals(object obj)
        {
            if (obj is Point)
            {
                Point point = (Point)obj;
                foreach (var v in point.Coordinates)
	                if ( == point.X && this.Y == point.Y)
	                {
	                    return (true);
	                }
            }

            return (false);
        }
        
*/
        public override string ToString()
        {
        	StringBuilder result = new StringBuilder();
        	result.Append("{ ");
        	foreach ( var v in Coordinates)
        		result.Append(v.ToString() + " ");
        	result.Append("}"); 	
        	
        	return result.ToString();
        }
        
        

        public virtual double DistanceTo(Point other)
        {
        	//try
            double summ = 0;
            for (int i = 0; i < Dimension; i++)
            	summ += Math.Pow(Coordinates[i] - other.Coordinates[i], 2);
            return Math.Sqrt(summ);
        }  
    }
}
